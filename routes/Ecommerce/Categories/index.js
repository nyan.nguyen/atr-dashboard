import React from 'react';
import Widget from "../../../app/components/Widget";
import IntlMessages from '../../../util/IntlMessages';
import { notification, Popconfirm, Space, Table } from 'antd';
import { CategoriesApi, SuppliersApi } from '../../../api';
import { useCookies } from 'react-cookie';
import { useRouter } from 'next/router';
import { useIntl } from 'react-intl';
import { DASHBOARD_PATH } from '../../../config';

const Categories = ({props}) => {
	const {categories} = props;
    const [cookie] = useCookies();
	const router = useRouter();
	const intl = useIntl();

	const onDelete = (_id) => {
		CategoriesApi.remove({
			_id: _id
		}, {
			headers: {
				"x-auth-token" :cookie.token
			}
		}).then(response => {
			if(response.errors) {
				console.log(response.errors);
				notification['error']({
					message: intl.formatMessage({id: "error.occured"})
				})
			} else {
				notification['success']({
					message: intl.formatMessage({id: "category.delete.success"})
				});
				router.replace(router.asPath);
			}
		})
	}

	const columns = [
		{
            title: <IntlMessages id="table.column.name"/>,
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: <IntlMessages id="table.column.description"/>,
            dataIndex: 'description',
            key: 'description',
        },
		{
			title: <IntlMessages id="table.column.action"/>,
			key: 'action',
			render: (text, record) => (
			  <Space size="middle">
				<a onClick={() => router.push(`/${DASHBOARD_PATH}ecommerce/edit-category/${record._id}`)} href="#"><IntlMessages id="button.modify" /></a>
				<Popconfirm
					title={<IntlMessages id="confirm.delete.message" />}
					onConfirm={() => onDelete(record._id)}
					okText={<IntlMessages id="button.yes" />}
					cancelText={<IntlMessages id="button.cancel" />}
				>
					<a href="#"><IntlMessages id="button.delete" /></a>
				</Popconfirm>
			  </Space>
			),
		},
	]

	return (
		<Widget styleName="gx-card-profile" extra={<a onClick={() => router.push(`/${DASHBOARD_PATH}ecommerce/create-category`)} href="#"><IntlMessages id="button.add"/></a>} title={<IntlMessages id="sidebar.ecommerce.categories"/>}>
            <Table className="gx-table-responsive" columns={columns} dataSource={categories}/>
        </Widget>
	);
};

export default Categories;
