import React from 'react';
import Widget from "../../../app/components/Widget";
import IntlMessages from '../../../util/IntlMessages';
import { Image, notification, Popconfirm, Space, Table } from 'antd';
import { ProductsApi } from '../../../api';
import { useCookies } from 'react-cookie';
import { useRouter } from 'next/router';
import { useIntl } from 'react-intl';
import { DASHBOARD_PATH } from '../../../config';

const Products = ({props}) => {
	const {products} = props;
    const [cookie] = useCookies();
	const router = useRouter();
	const intl = useIntl();

	const onDelete = (_id) => {
		ProductsApi.remove({
			_id: _id
		}, {
			headers: {
				"x-auth-token" :cookie.token
			}
		}).then(response => {
			if(response.errors) {
				console.log(response.errors);
				notification['error']({
					message: intl.formatMessage({id: "error.occured"})
				})
			} else {
				notification['success']({
					message: intl.formatMessage({id: "product.delete.success"})
				});
				router.replace(router.asPath);
			}
		})
	}

	const columns = [
		{
            title: <IntlMessages id="table.column.cover_photo"/>,
            dataIndex: 'cover',
            key: 'cover',
			render: (record) => {
				return <Image width={50} src={record}/>
			}
        },
		{
            title: <IntlMessages id="table.column.name"/>,
            dataIndex: 'name',
            key: 'name',
			render: (record) => {
				return record[0].value
			}
        },
		{
            title: <IntlMessages id="table.column.price"/>,
            dataIndex: 'price_tax_included',
            key: 'price_tax_included',
        },
        {
            title: <IntlMessages id="table.column.stocks"/>,
            dataIndex: 'stock',
            key: 'stock',
        },
		{
			title: <IntlMessages id="table.column.action"/>,
			key: 'action',
			render: (text, record) => (
			  <Space size="middle">
				<a onClick={() => router.push(`/${DASHBOARD_PATH}ecommerce/edit-product/${record._id}`)} href="#"><IntlMessages id="button.modify" /></a>
				<Popconfirm
					title={<IntlMessages id="confirm.delete.message" />}
					onConfirm={() => onDelete(record._id)}
					okText={<IntlMessages id="button.yes" />}
					cancelText={<IntlMessages id="button.cancel" />}
				>
					<a href="#"><IntlMessages id="button.delete" /></a>
				</Popconfirm>
			  </Space>
			),
		},
	]

	return (
		<Widget styleName="gx-card-profile" extra={<a onClick={() => router.push(`/${DASHBOARD_PATH}ecommerce/create-product`)} href="#"><IntlMessages id="button.add"/></a>} title={<IntlMessages id="sidebar.ecommerce.products"/>}>
            <Table className="gx-table-responsive" columns={columns} dataSource={products}/>
        </Widget>
	);
};

export default Products;
