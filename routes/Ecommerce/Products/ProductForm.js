import { Button, Col, Collapse, Form, Image, Input, InputNumber, notification, Row, Select, Spin, TreeSelect, Typography, Upload } from 'antd';
import React, { useEffect, useMemo, useState } from 'react';
import Widget from '../../../app/components/Widget';
import IntlMessages from '../../../util/IntlMessages';
import { MinusCircleOutlined, PlusOutlined, InboxOutlined } from '@ant-design/icons';
import { useIntl } from 'react-intl';
import Helpers from '../../../util/Helpers';
import { ProductsApi } from '../../../api';
import { useCookies } from 'react-cookie';
import { useRouter } from 'next/router';
import { DASHBOARD_PATH } from '../../../config';
import languageData from '../../../app/core/Topbar/languageData';
import { useQuill } from "react-quilljs";
import { LoadingOutlined } from '@ant-design/icons';

const ProductForm = ({props}) => {    
    const { quill: descriptionQuill, quillRef: descriptionQuillRef } = useQuill();
    const { quill: descriptionEnQuill, quillRef: descriptionEnQuillRef } = useQuill();
    const [certificationDescVN, setCertificationDescVN] = useState([]);
    const [certificationDescEN, setCertificationDescEN] = useState([]);
    const [certifications, setCertifications] = useState([]);
    const [certificationsImg, setCertificationsImg] = useState([]);
    
    // const { certificateQuill, certificateQuillRef } = useQuill();

    const intl = useIntl();
    const [coverPhoto, setCoverPhoto] = useState("");
    const [galery, setGalery] = useState([]);
    const [variationsSelectData, setVariationsSelectData] = useState([]);
    const [currentTaxSelected, setCurrentTaxSelected] = useState("");

    const [priceTaxExcluded, setPriceTaxExcluded] = useState(0);
    const [priceTaxIncluded, setPriceTaxIncluded] = useState(0);
    const [cookie] = useCookies();
    const router = useRouter();
    const [loading, setLoading] = useState(false);

    const { categories, taxes, variations, brands, suppliers, product } = props;
    
    useEffect(() => {
        if(product && Object.keys(product)){
            setGalery(product.galery);
            setCoverPhoto(product.cover);
            setCertificationsImg(product.certificationsImg);
            setCertifications(product.certifications)
        }
    },[product])

    useEffect(() => {
        if(descriptionQuill && product && Object.keys(product)){
            descriptionQuill.clipboard.dangerouslyPasteHTML(product.description[0].value)
        }
    },[descriptionQuill])

    useEffect(() => {
        if(descriptionEnQuill && product && Object.keys(product)){
            descriptionEnQuill.clipboard.dangerouslyPasteHTML(product.description[1].value)
        }
    },[descriptionEnQuill])

    const onSubmitForm = (values) => {
        setLoading(true);
        let tax = currentTaxSelected?taxes.find(t => t._id === currentTaxSelected).value:0;
        // console.log(values)
        // console.log({
        //     ...product,
        //     ...values,
        //     description: [{
        //         locale: "vn",
        //         value: descriptionQuill.root.innerHTML
        //     },{
        //         locale: "en",
        //         value: descriptionEnQuill.root.innerHTML
        //     }],
        //     instruction: [],
        //     certificationsImg: certificationsImg,
        //         certifications: certificationsImg.map((img,idx) => {
        //             return [{
        //                 locale: "vn",
        //                 value: certificationDescVN[idx]?certificationDescVN[idx]:""
        //             },{
        //                 locale: "en",
        //                 value: certificationDescEN[idx]?certificationDescEN[idx]:""
        //             }]
        //         }),
        //     cover: coverPhoto,
        //     galery: galery,
        //     price_tax_excluded: values.price_tax_included / (1+(tax/100)),
        //     variants: values.variants.stock?[]:values.variants, 
        //     stock: values.variants.stock?values.variants.stock:values.variants.reduce(((a,b) => a+ parseInt(b.stock)),0)
        // });
        if(!product) {
            console.log("create")
            ProductsApi.create({
                ...values,
                description: [{
                    locale: "vn",
                    value: descriptionQuill.root.innerHTML
                },{
                    locale: "en",
                    value: descriptionEnQuill.root.innerHTML
                }],
                instruction: [],
                certificationsImg: certificationsImg,
                certifications: certificationsImg.map((img,idx) => {
                    return [{
                        locale: "vn",
                        value: certificationDescVN[idx]?certificationDescVN[idx]:""
                    },{
                        locale: "en",
                        value: certificationDescEN[idx]?certificationDescEN[idx]:""
                    }]
                }),
                cover: coverPhoto,
                galery: galery,
                price_tax_excluded: values.price_tax_included / (1+(tax/100)),
                variants: values.variants.stock?[]:values.variants,
                // stock: values.variants.stock?values.variants.stock:values.variants.reduce(((a,b) => a+ parseInt(b.stock)),0)
                stock: 0
            }, {
                headers: {
                    "x-auth-token" :cookie.token
                }
            })
            .then(response => {
                setLoading(false);
                // console.log(response)
                if(response.errors) {
                    console.log(response.errors);
                    notification['error']({
                        message: intl.formatMessage({id: "error.occured"})
                    })
                } else {
                    notification['success']({
                        message: intl.formatMessage({id: "product.create.success"})
                    });
                    router.push(`/${DASHBOARD_PATH}ecommerce/products`)
                }
            })
        } else {
            // console.log("update")
            ProductsApi.update({
                ...product,
                ...values,
                description: [{
                    locale: "vn",
                    value: descriptionQuill.root.innerHTML
                },{
                    locale: "en",
                    value: descriptionQuill.root.innerHTML
                }],
                instruction: [],
                certificationsImg: certificationsImg,
                certifications: certificationsImg.map((img,idx) => {
                    return [{
                        locale: "vn",
                        value: certificationDescVN[idx]?certificationDescVN[idx]:""
                    },{
                        locale: "en",
                        value: certificationDescEN[idx]?certificationDescEN[idx]:""
                    }]
                }),
                cover: coverPhoto,
                galery: galery,
                price_tax_excluded: values.price_tax_included / (1+(tax/100)),
                variants: values.variants.stock?[]:values.variants, 
                stock: values.variants.stock?values.variants.stock:values.variants.reduce(((a,b) => a+ parseInt(b.stock)),0)
            },{
                headers: {
                    "x-auth-token" :cookie.token
                }
            })
            .then(response => {
                setLoading(false);
                if(response.errors) {
                    console.log(response.errors);
                    notification['error']({
                        message: intl.formatMessage({id: "error.occured"})
                    })
                } else {
                    notification['success']({
                        message: intl.formatMessage({id: "product.update.success"})
                    });
                    router.push(`/${DASHBOARD_PATH}ecommerce/products`)
                }
            })
        }
    }

    useEffect(() => {
        // console.log(product)
        // console.log(variations)
        if(variations.length) {
            setVariationsSelectData(
                variations.map(variation => ({
                    title: variation.name,
                    key: variation._id,
                    value: variation._id,
                    selectable: false,
                    checkable: false,
                    children: variation.values.length?variation.values.map(value => (
                        {
                            title: value.name,
                            value: variation._id+"#"+value._id,
                            key: variation._id+"#"+value._id
                        }
                    )):[]
                }))
            )
        }
    },[variations])

    const coverUploadProps = {
        name: 'cover',
        multiple: false,
        fileList: coverPhoto?[{
            uid: 'cover_photo',
            name: 'cover_photo',
            url: coverPhoto
        }]:[],
        listType:"picture",
        beforeUpload : async(file) => {
            const base64 = await Helpers.toBase64(file);
            setCoverPhoto(base64);
            console.log(base64)
            // Prevent upload
            return true;
        },
        onRemove: (file) => {
            setCoverPhoto("");
        }
    };

    const galeryUploadProps = {
        name: 'galery',
        multiple: true,
        listType:"picture",
        fileList: galery.length?galery.map((galery,idx) => ({
            uid: idx,
            name: 'Galery Photo '+idx,
            url: galery
        })):[],
        beforeUpload : async(file) => {
            const base64 = await Helpers.toBase64(file);
            setGalery(oldGalery => [...oldGalery,base64]);
            // Prevent upload
            console.log(galery)
            return true;
        },
        onRemove: (file) => {
            const index = galery.findIndex((photo,idx) => file.name==="Galery Photo "+idx);
            let tmp = [...galery];
            tmp.splice(index, 1);
            setGalery(tmp);
        }
    };

    const certificationsUploadProps = {
        name: 'certifications',
        multiple: true,
        listType:"picture",
        fileList: certificationsImg.length?certificationsImg.map((img,idx) => ({
            uid: idx,
            name: 'Certification '+idx,
            url: img
        })):[],
        showUploadList: false,
        beforeUpload : async(file) => {
            const base64 = await Helpers.toBase64(file);
            setCertificationsImg(oldImg => [...oldImg,base64]);
            // Prevent upload
            return true;
        },
        onRemove: (file) => {
            const index = certificationsImg.findIndex((photo,idx) => file.name==="Certification "+idx);
            let tmp = [...certificationsImg];
            tmp.splice(index, 1);
            setCertificationsImg(tmp);
        }
    };
  
    const antIcon = <LoadingOutlined style={{ fontSize: 20, color: "white" }} spin />;

    const onCertificationDescChanged = (e,idx,lang) => {
        if(lang==="VN") {
            let tmp = [...certificationDescVN];
            tmp[idx]=e.target.value;
            setCertificationDescVN(tmp);
        }
        if(lang==="EN") {
            let tmp = [...certificationDescEN];
            tmp[idx]=e.target.value;
            setCertificationDescEN(tmp);
        }
    }

    const onDeleteCert = (idx) => {
        let tmp = [...certificationsImg];
        tmp.splice(idx, 1);
        setCertificationsImg(tmp);

        tmp = [...certificationDescVN];
        if(certificationDescVN[idx]){
            tmp.splice(idx, 1);
            setCertificationDescVN(tmp);
        }

        tmp = [...certificationDescEN];
        if(certificationDescEN[idx]){
            tmp.splice(idx, 1);
            setCertificationDescEN(tmp);
        }
    }
      
	return (
        <Form
            layout="vertical" 
            onFinish={onSubmitForm}
            initialValues={{
                ...product
            }}
        >
            <Row style={{flexDirection: "row"}}>
                <Col xl={16} lg={14} md={14} sm={24} xs={24}>
                    <Row>
                        <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                            <Widget title={<IntlMessages id="product.form.product_information"/>} styleName="gx-card-profile">
                                <div className="gx-mb-2">
                                    <Typography.Text><IntlMessages id="form.name"/></Typography.Text>
                                </div>
                                <Form.List name="name">
                                    {(fields, { add, remove }) => {
                                        return (
                                            <div>
                                                {fields.map((field,index) => (
                                                    <Row key={index} style={{flexDirection: "row"}}>
                                                        <Col xl={4} lg={4} md={24} sm={24} xs={24}>
                                                            <Form.Item name={[index,"locale"]} label={<IntlMessages id="form.locale"/>}>
                                                                <Select>
                                                                    {languageData.map((language,idx) => (
                                                                        <Select.Option key={idx} value={language.code}>{language.name}</Select.Option>
                                                                    ))}
                                                                </Select>
                                                            </Form.Item>
                                                        </Col>
                                                        <Col xl={18} lg={18} md={24} sm={24} xs={24}>
                                                            <Form.Item name={[index,"value"]} label={<IntlMessages id="form.name"/>}>
                                                                <Input/>
                                                            </Form.Item>
                                                        </Col>
                                                        <Col xl={2} lg={2} md={24} sm={24} xs={24} style={{placeSelf:"center"}}>
                                                            <MinusCircleOutlined
                                                                className="dynamic-delete-button"
                                                                style={{ margin: '0 8px' }}
                                                                onClick={() => {
                                                                    remove(field.name);
                                                                }}
                                                            />
                                                        </Col>
                                                    </Row>
                                                ))}
                                                <Row>
                                                    <Col xl={fields.length>0?22:24} lg={fields.length>0?22:24} md={24} sm={24} xs={24}>
                                                        <Form.Item>
                                                            <Button
                                                                type="dashed"
                                                                onClick={() => {
                                                                    add();
                                                                }}
                                                                style={{ width: '100%' }}
                                                            >
                                                                <PlusOutlined /> <IntlMessages id="button.add"/>
                                                            </Button>
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </div>
                                        )
                                    }}
                                </Form.List>

                                <div className="gx-mb-2">
                                    <Typography.Text><IntlMessages id="form.short_description"/></Typography.Text>
                                </div>
                                <Form.List name="short_description">
                                    {(fields, { add, remove }) => {
                                        return (
                                            <div>
                                                {fields.map((field,index) => (
                                                    <Row key={index} style={{flexDirection: "row"}}>
                                                        <Col xl={4} lg={4} md={24} sm={24} xs={24}>
                                                            <Form.Item name={[index,"locale"]} label={<IntlMessages id="form.locale"/>}>
                                                                <Select>
                                                                    {languageData.map((language,idx) => (
                                                                        <Select.Option key={idx} value={language.code}>{language.name}</Select.Option>
                                                                    ))}
                                                                </Select>
                                                            </Form.Item>
                                                        </Col>
                                                        <Col xl={18} lg={18} md={24} sm={24} xs={24}>
                                                            <Form.Item name={[index,"value"]} label={<IntlMessages id="form.name"/>}>
                                                                <Input.TextArea rows={4}/>
                                                            </Form.Item>
                                                        </Col>
                                                        <Col xl={2} lg={2} md={24} sm={24} xs={24} style={{placeSelf:"center"}}>
                                                            <MinusCircleOutlined
                                                                className="dynamic-delete-button"
                                                                style={{ margin: '0 8px' }}
                                                                onClick={() => {
                                                                    remove(field.name);
                                                                }}
                                                            />
                                                        </Col>
                                                    </Row>
                                                ))}
                                                <Row>
                                                    <Col xl={fields.length>0?22:24} lg={fields.length>0?22:24} md={24} sm={24} xs={24}>
                                                        <Form.Item>
                                                            <Button
                                                                type="dashed"
                                                                onClick={() => {
                                                                    add();
                                                                }}
                                                                style={{ width: '100%' }}
                                                            >
                                                                <PlusOutlined /> <IntlMessages id="button.add"/>
                                                            </Button>
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </div>
                                        )
                                    }}
                                </Form.List>
                                
                                <div className="gx-mb-2">
                                    <Typography.Text><IntlMessages id="form.description"/> (VN)</Typography.Text>
                                </div>
                                <div style={{ marginBottom: "80px" }}>
                                    <div style={{ width: "600px", height: "300px", display: "block" }}>
                                        <div ref={descriptionQuillRef} />
                                    </div>
                                </div>

                                <div className="gx-mb-2">
                                    <Typography.Text><IntlMessages id="form.description"/> (EN)</Typography.Text>
                                </div>
                                <div style={{ marginBottom: "80px" }}>
                                    <div style={{ width: "600px", height: "300px", display: "block" }}>
                                        <div ref={descriptionEnQuillRef} />
                                    </div>
                                </div>                                
                            </Widget>        
                        </Col>

                        <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                            <Widget title={<IntlMessages id="product.form.inventory"/>} styleName="gx-card-profile">
                                <Form.List name="variants">
                                    {(fields, { add, remove }) => {
                                        return (
                                            <div>
                                                {fields.map((field,index) => (
                                                    <Row key={index} style={{flexDirection: "row"}}>
                                                        <Col xl={13} lg={11} md={24} sm={24} xs={24}>
                                                            <Form.Item name={[index,"attributeGroups"]} label={<IntlMessages id="product.form.attribute"/>}>
                                                                <TreeSelect 
                                                                    treeData={variationsSelectData}
                                                                    treeCheckable
                                                                />
                                                            </Form.Item>
                                                        </Col>
                                                        <Col xl={9} lg={11} md={24} sm={24} xs={24}>
                                                            <Form.Item name={[index,"images"]} label={<IntlMessages id="product.form.photo"/>}>
                                                                <Select 
                                                                    mode="multiple"
                                                                    allowClear
                                                                    showSearch
                                                                >
                                                                    {galery.map((photo,idx) => (
                                                                        <Select.Option key={idx} value={idx}>{`Galery Photo ${idx}`}</Select.Option>
                                                                    ))}
                                                                </Select>
                                                            </Form.Item>
                                                        </Col>
                                                        <Col xl={2} lg={2} md={24} sm={24} xs={24} style={{placeSelf:"center"}}>
                                                            <MinusCircleOutlined
                                                                className="dynamic-delete-button"
                                                                style={{ margin: '0 8px' }}
                                                                onClick={() => {
                                                                    remove(field.name);
                                                                }}
                                                            />
                                                        </Col>
                                                        <Col>
                                                            <Form.Item name={[index,"stock"]} initialValue="0" hidden label={<IntlMessages id="product.form.quantity"/>}>
                                                                <Input type="number"/>
                                                            </Form.Item>
                                                        </Col>
                                                    </Row>
                                                ))}
                                                <Row>
                                                    <Col xl={fields.length>0?22:24} lg={fields.length>0?22:24} md={24} sm={24} xs={24}>
                                                        <Form.Item>
                                                            <Button
                                                                type="dashed"
                                                                onClick={() => {
                                                                    add();
                                                                }}
                                                                style={{ width: '100%' }}
                                                            >
                                                                <PlusOutlined /> <IntlMessages id="product.form.add_new_variation"/>
                                                            </Button>
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </div>
                                        )
                                    }}
                                </Form.List>
                            </Widget>        
                        </Col>
                    
                        <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                            <Widget title={<IntlMessages id="form.instruction"/>} styleName="gx-card-profile">
                                <Upload.Dragger {...certificationsUploadProps}>
                                    <p className="ant-upload-drag-icon">
                                        <InboxOutlined />
                                    </p>
                                </Upload.Dragger>
                                    {certificationsImg.map((img,idx) => (
                                        <Row gutter={16} key={`certs_${img}`}>
                                            <Col span={8}>
                                                <Image width={"100%"} src={img}/>
                                                <Button type="link" danger onClick={() => onDeleteCert(idx)}>
                                                    <IntlMessages id={"button.delete"}/>
                                                </Button>
                                            </Col>
                                            <Col span={24}>
                                                <Typography.Text><IntlMessages id="form.description"/> (VN)</Typography.Text>
                                                <Input.TextArea onChange={e => onCertificationDescChanged(e,idx,"VN")} rows={4} defaultValue={certifications[idx]?certifications[idx][0].value:""}/>
                                                <Typography.Text><IntlMessages id="form.description"/> (EN)</Typography.Text>
                                                <Input.TextArea onChange={e => onCertificationDescChanged(e,idx,"EN")} rows={4} defaultValue={certifications[idx]?certifications[idx][1].value:""}/>
                                            </Col>
                                        </Row>
                                    ))}
                            </Widget>        
                        </Col>

                        <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                            <Widget title={<IntlMessages id="product.form.galery"/>}>
                                <Upload.Dragger {...galeryUploadProps}>
                                    <p className="ant-upload-drag-icon">
                                        <InboxOutlined />
                                    </p>
                                </Upload.Dragger>
                            </Widget>
                        </Col>
                    </Row>
                </Col>
            
                <Col xl={8} lg={10} md={10} sm={24} xs={24}>
                    <Row>
                        <Col xl={24} lg={24} md={24} sm={12} xs={24}>
                            <Widget title={<IntlMessages id="product.form.cover_photo"/>}>
                                <Upload.Dragger {...coverUploadProps}>
                                    <p className="ant-upload-drag-icon">
                                        <InboxOutlined />
                                    </p>
                                </Upload.Dragger>
                            </Widget>
                        </Col>

                        {/* <Col xl={24} lg={24} md={24} sm={12} xs={24}>
                            <Widget title={<IntlMessages id="product.form.price"/>}>
                                <Form.Item name="taxId" label={<IntlMessages id="form.tax"/>}>
                                    <Select showSearch onChange={(value) => setCurrentTaxSelected(value)}>
                                        {taxes.map(tax => (
                                            <Select.Option key={tax._id} value={tax._id}>{tax.name} ({tax.value}%)</Select.Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                                <Form.Item name="price_tax_included" label={<IntlMessages id="form.price_tax_included"/>}>
                                    <Input 
                                        style={{width: "100%"}}       
                                        suffix="VND"
                                    />
                                </Form.Item>
                            </Widget>
                        </Col> */}

                        <Col xl={24} lg={24} md={24} sm={12} xs={24}>
                            <Widget title={<IntlMessages id="product.form.reference"/>}>
                                <Form.Item name="sku" label={<IntlMessages id="form.sku"/>}>
                                    <Input/>
                                </Form.Item>
                                <Form.Item name="brandId" label={<IntlMessages id="form.brand"/>}>
                                    <Select 
                                        allowClear
                                        showSearch
                                    >
                                        {brands.map(brand => (
                                            <Select.Option key={brand._id} value={brand._id}>{brand.name}</Select.Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                                <Form.Item name="supplyId" label={<IntlMessages id="form.supplier"/>}>
                                    <Select 
                                        allowClear
                                        showSearch
                                    >
                                        {suppliers.map(supplier => (
                                            <Select.Option key={supplier._id} value={supplier._id}>{supplier.name}</Select.Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                                <Form.Item name="categoryId" label={<IntlMessages id="form.category"/>}>
                                    <Select 
                                        mode="multiple"
                                        allowClear
                                        showSearch
                                    >
                                        {categories.map(category => (
                                            <Select.Option key={category._id} value={category._id}>{category.name}</Select.Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                            </Widget>
                        </Col>

                        <Col xl={24} lg={24} md={24} sm={12} xs={24}>
                            <Form.Item>
                                <Button type="primary" htmlType="submit" style={{width: "100%"}} disabled={loading}>
                                    {loading?<Spin className="gx-pt-2" indicator={antIcon}/>:<IntlMessages id="button.submit"/>}
                                </Button>
                            </Form.Item>
                        </Col>
                    </Row>
                </Col>
            
            </Row>
        </Form>    
	
    );
};

export default ProductForm;
