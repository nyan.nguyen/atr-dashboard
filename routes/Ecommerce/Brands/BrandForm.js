import React from 'react';
import Widget from "../../../app/components/Widget";
import IntlMessages from '../../../util/IntlMessages';
import { Button, Form, Input, notification } from 'antd';
import { useCookies } from 'react-cookie';
import { BrandsApi } from '../../../api';
import { useRouter } from 'next/router';
import { useIntl } from 'react-intl';
import { DASHBOARD_PATH } from '../../../config';

const BrandForm = ({props}) => {
    const brand = props?.brand;
    const intl = useIntl();
    const [cookie] = useCookies();
    const router = useRouter();

    console.log(props,brand);
    
    const formItemLayout = {
        labelCol: {
          span: 6,
        },
        wrapperCol: {
          span: 14,
        },
    };

    const onSubmitForm = (values) => {
        if(!brand) {
            BrandsApi.createBrand(values, {
                headers: {
                    "x-auth-token" :cookie.token
                }
            })
            .then(response => {
                if(response.errors) {
                    console.log(response.errors);
                    notification['error']({
                        message: intl.formatMessage({id: "error.occured"})
                    })
                } else {
                    notification['success']({
                        message: intl.formatMessage({id: "brand.create.success"})
                    });
                    router.push(`/${DASHBOARD_PATH}ecommerce/brands`)
                }
            })
        } else {
            BrandsApi.updateBrand({
                ...brand,
                ...values
            },{
                headers: {
                    "x-auth-token" :cookie.token
                }
            })
            .then(response => {
                if(response.errors) {
                    console.log(response.errors);
                    notification['error']({
                        message: intl.formatMessage({id: "error.occured"})
                    })
                } else {
                    notification['success']({
                        message: intl.formatMessage({id: "brand.update.success"})
                    });
                    router.push(`/${DASHBOARD_PATH}ecommerce/brands`)
                }
            })
        }
    }

	return (
		<Widget styleName="gx-card-profile">
            <div className="ant-card-head">
                <span className="ant-card-head-title gx-mb-2">
                    {brand?
                        <IntlMessages id="brands.form.edit_title"/>
                    :
                        <IntlMessages id="brands.form.create_title"/>
                    }
                </span>
            </div>
            <Form
                {...formItemLayout}
                onFinish={onSubmitForm}
                initialValues={{
                    name: brand?brand.name:""
                }}
            >
                <Form.Item 
                    name="name" 
                    label={<IntlMessages id="form.name"/>}
                    rules={[{ required: true, message: <IntlMessages id="validation.field.required"/> }]}
                >
                    <Input/>
                </Form.Item>   
                <Form.Item
                    wrapperCol={{
                        span: 20,
                        offset: 6,
                    }}
                >
                    <Button type="primary" htmlType="submit" style={{float: "right"}}>
                        <IntlMessages id="button.submit"/> 
                    </Button>
                </Form.Item>
            </Form>
        </Widget>
	);
};

export default BrandForm;
