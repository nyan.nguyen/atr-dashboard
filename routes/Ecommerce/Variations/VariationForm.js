import React from 'react';
import Widget from "../../../app/components/Widget";
import IntlMessages from '../../../util/IntlMessages';
import { Button, Card, Col, Form, Input, notification, Radio, Row } from 'antd';
import { useCookies } from 'react-cookie';
import { VariationsApi } from '../../../api';
import { useRouter } from 'next/router';
import { useIntl } from 'react-intl';
import { DASHBOARD_PATH } from '../../../config';
import {PlusOutlined,MinusCircleOutlined} from '@ant-design/icons';
import Helpers from '../../../util/Helpers';

const VariationForm = ({props}) => {
    const variation = props?.variation;
    const intl = useIntl();
    const [cookie] = useCookies();
    const router = useRouter();
    
    const onSubmitForm = (values) => {
        if(!variation) {
            VariationsApi.create(values, {
                headers: {
                    "x-auth-token" :cookie.token
                }
            })
            .then(response => {
                if(response.errors) {
                    console.log(response.errors);
                    notification['error']({
                        message: intl.formatMessage({id: "error.occured"})
                    })
                } else {
                    notification['success']({
                        message: intl.formatMessage({id: "variation.create.success"})
                    });
                    router.push(`/${DASHBOARD_PATH}ecommerce/variations`)
                }
            })
        } else {
            VariationsApi.update({
                ...variation,
                ...values
            },{
                headers: {
                    "x-auth-token" :cookie.token
                }
            })
            .then(response => {
                if(response.errors) {
                    console.log(response.errors);
                    notification['error']({
                        message: intl.formatMessage({id: "error.occured"})
                    })
                } else {
                    notification['success']({
                        message: intl.formatMessage({id: "variation.update.success"})
                    });
                    router.push(`/${DASHBOARD_PATH}ecommerce/variations`)
                }
            })
        }
    }

	return (
		<Widget styleName="gx-card-profile">
            <div className="ant-card-head">
                <span className="ant-card-head-title gx-mb-2">
                    {variation?
                        <IntlMessages id="variations.form.edit_title"/>
                    :
                        <IntlMessages id="variations.form.create_title"/>
                    }
                </span>
            </div>
            <Form 
                layout="vertical" 
                hideRequiredMark 
                onFinish={onSubmitForm}
                initialValues={{...variation}}
            >
				<Row gutter={16}>
					<Col span={24}>
						<Form.Item
							name="name"
							label={intl.formatMessage({ id: "form.name" })}
							rules={[{
								required: true,
								message: intl.formatMessage({ id: "validation.field.required" })
							}]}
						>
							<Input/>
						</Form.Item>
					</Col>
				</Row>
				<Row gutter={16}>
					<Col span={24}>
						<Form.Item
							label={intl.formatMessage({ id: "form.type" })}
                            name="type"
						>
							<Radio.Group
								buttonStyle="solid"
							>
								<Radio.Button value={1}><IntlMessages id="form.radio"/></Radio.Button>
								<Radio.Button value={2}><IntlMessages id="form.multiple_choice"/></Radio.Button>
								<Radio.Button value={3}><IntlMessages id="form.drop_down"/></Radio.Button>
							</Radio.Group>
						</Form.Item>
					</Col>
				</Row>

				<Row gutter={16}>
					<Col span={24}>
						<Widget>
                            <div className="ant-card-head">
                                <span className="ant-card-head-title gx-mb-2">
                                    <IntlMessages id="variations.form.values"/>
                                </span>
                            </div>
							<Form.List name="values" >
								{(fields, { add, remove }) => {
									return (
										<div>
											{fields.map((field, index) => (
												<Row key={field.key} gutter={16}>
													<Col sm={24} md={7} hidden={true} F>
														<Form.Item
															{...field}
															name={[field.name, '_id']}
															fieldKey={[field.fieldKey, '_id']} initialValue={Helpers.createUID(10)}
														>
															<Input />
														</Form.Item>
													</Col>
													<Col sm={24} md={7}>
														<Form.Item
															{...field}
															label={intl.formatMessage({ id: "form.name" })}
															name={[field.name, 'name']}
															fieldKey={[field.fieldKey, 'name']}
															rules={[{ required: true, message: 'validation.field.required' }]}
														>
															<Input />
														</Form.Item>
													</Col>
													<Col sm={24} md={7}>
														<Form.Item
															{...field}
															label={intl.formatMessage({ id: "form.value" })}
															name={[field.name, 'value']}
															fieldKey={[field.fieldKey, 'value']}
														>
															<Input />
														</Form.Item>
													</Col>
													<Col sm={24} md={7}>
														<Form.Item
															{...field}
															label={intl.formatMessage({ id: "form.description" })}
															name={[field.name, 'description']}
															fieldKey={[field.fieldKey, 'description']}
														>
															<Input.TextArea />
														</Form.Item>
													</Col>
													<Col sm={24} md={2}>
														<MinusCircleOutlined className="mt-md-4 pt-md-3" onClick={() => { remove(field.name) }} />
													</Col>
													<Col span={24}>
														<hr className="mt-2" />
													</Col>
												</Row>
											))}
											<Form.Item>
												<Button type="dashed" onClick={() => { add() }} className="w-100">
													<PlusOutlined /> {intl.formatMessage({ id: "button.add" })}
												</Button>
											</Form.Item>
										</div>
									);
								}}
							</Form.List>
						</Widget>
					</Col>
				</Row>
			
                <Row gutter={16}>
					<Col span={24}>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" style={{float: "right"}}>
                                <IntlMessages id="button.submit"/> 
                            </Button>
                        </Form.Item>
					</Col>
				</Row>
            </Form>
        </Widget>
	);
};

export default VariationForm;
