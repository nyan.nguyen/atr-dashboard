import React from 'react';
import Widget from "../../../app/components/Widget";
import IntlMessages from '../../../util/IntlMessages';
import { Button, Col, Form, Input, notification, Row } from 'antd';
import { useCookies } from 'react-cookie';
import { EmployeesApi, SuppliersApi } from '../../../api';
import { useRouter } from 'next/router';
import { useIntl } from 'react-intl';
import { DASHBOARD_PATH } from '../../../config';

const EmployeeForm = ({props}) => {
    const employee = props?.employee;
    const intl = useIntl();
    const [cookie] = useCookies();
    const router = useRouter();
    
    const onSubmitForm = (values) => {
        if(!employee) {
            EmployeesApi.create(values, {
                headers: {
                    "x-auth-token" :cookie.token
                }
            })
            .then(response => {
                if(response.errors) {
                    console.log(response.errors);
                    notification['error']({
                        message: intl.formatMessage({id: "error.occured"})
                    })
                } else {
                    notification['success']({
                        message: intl.formatMessage({id: "employee.create.success"})
                    });
                    router.push(`/${DASHBOARD_PATH}accounts/employees`)
                }
            })
        } else {
            EmployeesApi.update({
                ...employee,
                ...values
            },{
                headers: {
                    "x-auth-token" :cookie.token
                }
            })
            .then(response => {
                if(response.errors) {
                    console.log(response.errors);
                    notification['error']({
                        message: intl.formatMessage({id: "error.occured"})
                    })
                } else {
                    notification['success']({
                        message: intl.formatMessage({id: "employee.update.success"})
                    });
                    router.push(`/${DASHBOARD_PATH}accounts/employees`)
                }
            })
        }
    }

	return (
		<Widget styleName="gx-card-profile">
            <div className="ant-card-head">
                <span className="ant-card-head-title gx-mb-2">
                    {employee?
                        <IntlMessages id="employees.form.edit_title"/>
                    :
                        <IntlMessages id="employees.form.create_title"/>
                    }
                </span>
            </div>
            <Form 
                layout="vertical" 
                hideRequiredMark
                onFinish={onSubmitForm}
                initialValues={{
                    ...employee
                }}
            >
				<Row gutter={16}>
					<Col span={24}>
						<Form.Item
							name="name"
							label={intl.formatMessage({ id: "form.name" })}
							rules={[{
								required: true,
								message: intl.formatMessage({ id: "validation.field.required" })
							}]}
						>
							<Input/>
						</Form.Item>
					</Col>
				</Row>
                <Row gutter={16}>
					<Col span={24}>
						<Form.Item
							name="email"
							label={intl.formatMessage({ id: "form.email" })}
							rules={[{
								required: true,
								message: intl.formatMessage({ id: "validation.field.required" })
							}]}
						>
							<Input type="email"/>
						</Form.Item>
					</Col>
				</Row>
                {!employee &&
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item
                                name="password"
                                label={intl.formatMessage({ id: "form.password" })}
                                rules={[{
                                    required: true,
                                    message: intl.formatMessage({ id: "validation.field.required" })
                                }]}
                            >
                                <Input type="password" placeholder="Password"/>
                            </Form.Item>
                        </Col>
                    </Row>
                }
                <Row gutter={16}>
					<Col span={24}>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" style={{float: "right"}}>
                                <IntlMessages id="button.submit"/> 
                            </Button>
                        </Form.Item>
					</Col>
				</Row>
			</Form>
		</Widget>
	);
};

export default EmployeeForm;
