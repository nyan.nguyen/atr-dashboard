import { Button, Col, Form, Input, InputNumber, notification, Row, Select, TreeSelect, Typography, Upload } from 'antd';
import React, { useEffect, useState } from 'react';
import Widget from '../../../app/components/Widget';
import IntlMessages from '../../../util/IntlMessages';
import { MinusCircleOutlined, PlusOutlined, InboxOutlined } from '@ant-design/icons';
import { useIntl } from 'react-intl';
import Helpers from '../../../util/Helpers';
import { InvestmentsApi } from '../../../api';
import { useCookies } from 'react-cookie';
import { useRouter } from 'next/router';
import { DASHBOARD_PATH } from '../../../config';
import languageData from '../../../app/core/Topbar/languageData';

const ProjectForm = ({props}) => {
    const intl = useIntl();
    const [coverPhoto, setCoverPhoto] = useState("");
    const [cookie] = useCookies();
    const router = useRouter();

    const { project } = props;
    
    useEffect(() => {
        if(project && Object.keys(project)){
            setCoverPhoto(project.cover);
        }
    },[project])

    const onSubmitForm = (values) => {
        console.log(values)
        if(!project) {
            console.log("create")
            InvestmentsApi.create({
                ...values,
                cover: coverPhoto,
            }, {
                headers: {
                    "x-auth-token" :cookie.token
                }
            })
            .then(response => {
                console.log(response)
                if(response.errors) {
                    console.log(response.errors);
                    notification['error']({
                        message: intl.formatMessage({id: "error.occured"})
                    })
                } else {
                    notification['success']({
                        message: intl.formatMessage({id: "project.create.success"})
                    });
                    router.push(`/${DASHBOARD_PATH}investment/projects`)
                }
            })
        } else {
            console.log("update")
            InvestmentsApi.update({
                ...project,
                ...values,
                cover: coverPhoto,
            },{
                headers: {
                    "x-auth-token" :cookie.token
                }
            })
            .then(response => {
                if(response.errors) {
                    console.log(response.errors);
                    notification['error']({
                        message: intl.formatMessage({id: "error.occured"})
                    })
                } else {
                    notification['success']({
                        message: intl.formatMessage({id: "project.update.success"})
                    });
                    router.push(`/${DASHBOARD_PATH}investment/projects`)
                }
            })
        }
    }

    const coverUploadProps = {
        name: 'cover',
        multiple: false,
        fileList: coverPhoto?[{
            uid: 'cover_photo',
            name: 'cover_photo',
            url: coverPhoto
        }]:[],
        listType:"picture",
        beforeUpload : async(file) => {
            const base64 = await Helpers.toBase64(file);
            setCoverPhoto(base64);
            console.log(base64)
            // Prevent upload
            return true;
        },
        onRemove: (file) => {
            setCoverPhoto("");
        }
    };  
      
	return (
        <Form
            layout="vertical" 
            onFinish={onSubmitForm}
            initialValues={{
                ...project
            }}
        >
            <Row style={{flexDirection: "row"}}>
                <Col xl={16} lg={14} md={14} sm={24} xs={24}>
                    <Row>
                        <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                            <Widget title={<IntlMessages id="project.form.project_information"/>} styleName="gx-card-profile">
                                <div className="gx-mb-2">
                                    <Typography.Text><IntlMessages id="form.name"/></Typography.Text>
                                </div>
                                <Form.List name="name">
                                    {(fields, { add, remove }) => {
                                        return (
                                            <div>
                                                {fields.map((field,index) => (
                                                    <Row key={index} style={{flexDirection: "row"}}>
                                                        <Col xl={4} lg={4} md={24} sm={24} xs={24}>
                                                            <Form.Item name={[index,"locale"]} label={<IntlMessages id="form.locale"/>}>
                                                                <Select>
                                                                    {languageData.map((language,idx) => (
                                                                        <Select.Option key={idx} value={language.code}>{language.name}</Select.Option>
                                                                    ))}
                                                                </Select>
                                                            </Form.Item>
                                                        </Col>
                                                        <Col xl={18} lg={18} md={24} sm={24} xs={24}>
                                                            <Form.Item name={[index,"value"]} label={<IntlMessages id="form.name"/>}>
                                                                <Input/>
                                                            </Form.Item>
                                                        </Col>
                                                        <Col xl={2} lg={2} md={24} sm={24} xs={24} style={{placeSelf:"center"}}>
                                                            <MinusCircleOutlined
                                                                className="dynamic-delete-button"
                                                                style={{ margin: '0 8px' }}
                                                                onClick={() => {
                                                                    remove(field.name);
                                                                }}
                                                            />
                                                        </Col>
                                                    </Row>
                                                ))}
                                                <Row>
                                                    <Col xl={fields.length>0?22:24} lg={fields.length>0?22:24} md={24} sm={24} xs={24}>
                                                        <Form.Item>
                                                            <Button
                                                                type="dashed"
                                                                onClick={() => {
                                                                    add();
                                                                }}
                                                                style={{ width: '100%' }}
                                                            >
                                                                <PlusOutlined /> <IntlMessages id="button.add"/>
                                                            </Button>
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </div>
                                        )
                                    }}
                                </Form.List>
                                                                
                                <div className="gx-mb-2">
                                    <Typography.Text><IntlMessages id="form.description"/></Typography.Text>
                                </div>
                                <Form.List name="description">
                                    {(fields, { add, remove }) => {
                                        return (
                                            <div>
                                                {fields.map((field,index) => (
                                                    <Row key={index} style={{flexDirection: "row"}}>
                                                        <Col xl={4} lg={4} md={24} sm={24} xs={24}>
                                                            <Form.Item name={[index,"locale"]} label={<IntlMessages id="form.locale"/>}>
                                                                <Select>
                                                                    {languageData.map((language,idx) => (
                                                                        <Select.Option key={idx} value={language.code}>{language.name}</Select.Option>
                                                                    ))}
                                                                </Select>
                                                            </Form.Item>
                                                        </Col>
                                                        <Col xl={18} lg={18} md={24} sm={24} xs={24}>
                                                            <Form.Item name={[index,"value"]} label={<IntlMessages id="form.name"/>}>
                                                                <Input.TextArea rows={6}/>
                                                            </Form.Item>
                                                        </Col>
                                                        <Col xl={2} lg={2} md={24} sm={24} xs={24} style={{placeSelf:"center"}}>
                                                            <MinusCircleOutlined
                                                                className="dynamic-delete-button"
                                                                style={{ margin: '0 8px' }}
                                                                onClick={() => {
                                                                    remove(field.name);
                                                                }}
                                                            />
                                                        </Col>
                                                    </Row>
                                                ))}
                                                <Row>
                                                    <Col xl={fields.length>0?22:24} lg={fields.length>0?22:24} md={24} sm={24} xs={24}>
                                                        <Form.Item>
                                                            <Button
                                                                type="dashed"
                                                                onClick={() => {
                                                                    add();
                                                                }}
                                                                style={{ width: '100%' }}
                                                            >
                                                                <PlusOutlined /> <IntlMessages id="button.add"/>
                                                            </Button>
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </div>
                                        )
                                    }}
                                </Form.List>
                            
                                <Form.Item name="hasCompleted" label={<IntlMessages id="form.processing"/>}>
                                    <Select>
                                        <Select.Option value={true}><IntlMessages id="project.status.completed"/></Select.Option>
                                        <Select.Option value={false}><IntlMessages id="project.status.processing"/></Select.Option>
                                    </Select>
                                </Form.Item>
                            </Widget>        
                        </Col>
                    </Row>
                </Col>
            
                <Col xl={8} lg={10} md={10} sm={24} xs={24}>
                    <Row>
                        <Col xl={24} lg={24} md={24} sm={12} xs={24}>
                            <Widget title={<IntlMessages id="project.form.cover_photo"/>}>
                                <Upload.Dragger {...coverUploadProps}>
                                    <p className="ant-upload-drag-icon">
                                        <InboxOutlined />
                                    </p>
                                </Upload.Dragger>
                            </Widget>
                        </Col>

                        <Col xl={24} lg={24} md={24} sm={12} xs={24}>
                            <Form.Item>
                                <Button type="primary" htmlType="submit" style={{width: "100%"}}>
                                    <IntlMessages id="button.submit"/> 
                                </Button>
                            </Form.Item>
                        </Col>
                    </Row>
                </Col>
            
            </Row>
        </Form>    
	);
};

export default ProjectForm;
