import React from 'react';
import asyncComponent from '../../../util/asyncComponent'

const EmployeeForm = asyncComponent(() => import('../../../routes/Accounts/Employees/EmployeeForm'));

const CreateEmployee = () => <EmployeeForm/>;

export default CreateEmployee;