import React from 'react';
import asyncComponent from '../../../util/asyncComponent'

const ClientsComponent = asyncComponent(() => import('../../../routes/Accounts/Clients'));

const Clients = () => <ClientsComponent/>;

export default Clients;
