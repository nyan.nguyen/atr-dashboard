import React from 'react';
import asyncComponent from '../../../util/asyncComponent'

const VariationForm = asyncComponent(() => import('../../../routes/Ecommerce/Variations/VariationForm'));

const CreateVariation = () => <VariationForm/>;

export default CreateVariation;