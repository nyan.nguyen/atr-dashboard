import React from 'react';
import { TaxesApi } from '../../../api';
import asyncComponent from '../../../util/asyncComponent'
import cookie from 'cookie';

const TaxesComponent = asyncComponent(() => import('../../../routes/Ecommerce/Taxes'));

const Taxes = (props) => <TaxesComponent props={props}/>;

export default Taxes;

export async function getServerSideProps(context) {
    const cookies =  context.req.headers.cookie?cookie.parse(context.req.headers.cookie):"";
    const taxes = await TaxesApi.getAll({
        headers: {
            "x-auth-token" :cookies.token
        }
    });
    
    return {props: {taxes: taxes?taxes:[]}}
}