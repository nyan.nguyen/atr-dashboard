import React from 'react';
import asyncComponent from '../../../util/asyncComponent'
import cookie from 'cookie';
import { VariationsApi } from '../../../api';

const VariationForm = asyncComponent(() => import('../../../routes/Ecommerce/Variations/VariationForm'));

const EditVariation = (props) => <VariationForm props={props}/>;

export default EditVariation;

export async function getServerSideProps(context) {
    const cookies =  context.req.headers.cookie?cookie.parse(context.req.headers.cookie):"";
    const variation = await VariationsApi.get({
        _id: context.query.vid
    },{
        headers: {
            "x-auth-token" :cookies.token
        }
    });
    
    return {props: {variation: variation?variation:null}}
}