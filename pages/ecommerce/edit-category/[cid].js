import React from 'react';
import asyncComponent from '../../../util/asyncComponent'
import cookie from 'cookie';
import { CategoriesApi } from '../../../api';

const CategoryForm = asyncComponent(() => import('../../../routes/Ecommerce/Categories/CategoryForm'));

const EditCategory = (props) => <CategoryForm props={props}/>;

export default EditCategory;

export async function getServerSideProps(context) {
    const cookies =  context.req.headers.cookie?cookie.parse(context.req.headers.cookie):"";
    const category = await CategoriesApi.get({
        _id: context.query.cid
    },{
        headers: {
            "x-auth-token" :cookies.token
        }
    });
    
    return {props: {category: category?category:null}}
}