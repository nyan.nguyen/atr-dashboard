import React from 'react';
import { BrandsApi, CategoriesApi, ProductsApi, SuppliersApi, TaxesApi, VariationsApi } from '../../../api';
import asyncComponent from '../../../util/asyncComponent'
import cookie from 'cookie';

const ProductForm = asyncComponent(() => import('../../../routes/Ecommerce/Products/ProductForm'));

const EditProduct = (props) => <ProductForm props={props}/>;

export default EditProduct;

export async function getServerSideProps(context) {
    const cookies =  context.req.headers.cookie?cookie.parse(context.req.headers.cookie):"";
    const product = await ProductsApi.get({
        _id: context.query.pid
    },{
        headers: {
            "x-auth-token" :cookies.token
        }
    });

    const taxes = await TaxesApi.getAll({
        headers: {
            "x-auth-token" :cookies.token
        }
    });

    const categories = await CategoriesApi.getAll({
        headers: {
            "x-auth-token" :cookies.token
        }
    });

    const variations = await VariationsApi.getAll({
        headers: {
            "x-auth-token" :cookies.token
        }
    });

    const brands = await BrandsApi.getBrands({
        headers: {
            "x-auth-token" :cookies.token
        }
    });

    const suppliers = await SuppliersApi.getAll({
        headers: {
            "x-auth-token" :cookies.token
        }
    });
    
    return {props: {
        product: product?product:{},
        taxes: taxes?taxes:[],
        categories: categories?categories:[],
        variations: variations?variations:[],
        brands: brands?brands:[],
        suppliers: suppliers?suppliers:[]
    }}
}