import React from 'react';
import asyncComponent from '../../../util/asyncComponent'

const CategoryForm = asyncComponent(() => import('../../../routes/Ecommerce/Categories/CategoryForm'));

const CreateCategory = () => <CategoryForm/>;

export default CreateCategory;