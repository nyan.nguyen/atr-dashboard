import React from 'react';
import { ProductsApi } from '../../../api';
import asyncComponent from '../../../util/asyncComponent'
import cookie from 'cookie';

const ProductsComponent = asyncComponent(() => import('../../../routes/Ecommerce/Products'));

const Products = (props) => <ProductsComponent props={props}/>;

export default Products;

export async function getServerSideProps(context) {
    const cookies =  context.req.headers.cookie?cookie.parse(context.req.headers.cookie):"";
    const products = await ProductsApi.getAll({
        headers: {
            "x-auth-token" :cookies.token
        }
    });
    
    return {props: {products: products?products:[]}}
}