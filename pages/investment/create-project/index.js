import React from 'react';
import asyncComponent from '../../../util/asyncComponent'

const ProjectForm = asyncComponent(() => import('../../../routes/Investment/Projects/ProjectForm'));

const CreateProject = (props) => <ProjectForm props={props}/>;

export default CreateProject;