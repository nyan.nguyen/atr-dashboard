import React from 'react';
import asyncComponent from '../../../util/asyncComponent'
import cookie from 'cookie';
import { InvestmentsApi } from '../../../api';

const ProjectForm = asyncComponent(() => import('../../../routes/Investment/Projects/ProjectForm'));

const EditProject = (props) => <ProjectForm props={props}/>;

export default EditProject;

export async function getServerSideProps(context) {
    const cookies =  context.req.headers.cookie?cookie.parse(context.req.headers.cookie):"";
    const project = await InvestmentsApi.get({
        _id: context.query.pid
    },{
        headers: {
            "x-auth-token" :cookies.token
        }
    });
    
    return {props: {project: project?project:null}}
}