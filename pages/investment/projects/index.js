import React from 'react';
import { InvestmentsApi } from '../../../api';
import asyncComponent from '../../../util/asyncComponent'
import cookie from 'cookie';

const ProjectsComponent = asyncComponent(() => import('../../../routes/Investment/Projects'));

const Projects = (props) => <ProjectsComponent props={props}/>;

export default Projects;

export async function getServerSideProps(context) {
    const cookies =  context.req.headers.cookie?cookie.parse(context.req.headers.cookie):"";
    const projects = await InvestmentsApi.getAll({
        headers: {
            "x-auth-token" :cookies.token
        }
    });
    
    return {props: {projects: projects?projects:[]}}
}