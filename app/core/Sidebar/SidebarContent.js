import React, {useEffect} from "react";
import {Menu} from "antd";
import Link from "next/link";

import {useRouter} from 'next/router'
import CustomScrollbars from "../../../util/CustomScrollbars";
import SidebarLogo from "./SidebarLogo";
import UserProfile from "./UserProfile";
import AppsNavigation from "./AppsNavigation";
import {
  NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR,
  NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
  THEME_TYPE_LITE
} from "../../../constants/ThemeSetting";
import {useDispatch, useSelector} from "react-redux";
import {setPathName} from "../../../redux/actions";
import IntlMessages from "../../../util/IntlMessages";
import { DASHBOARD_PATH } from "../../../config";

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;


const SidebarContent = () => {

  const dispatch = useDispatch();
  const router = useRouter()
  let {navStyle, themeType, pathname} = useSelector(({settings}) => settings);

  const getNoHeaderClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR || navStyle === NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR) {
      return "gx-no-header-notifications";
    }
    return "";
  };

  const getNavStyleSubMenuClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR) {
      return "gx-no-header-submenu-popup";
    }
    return "";
  };

  useEffect(() => {
    dispatch(setPathName(router.pathname))
  }, [router.pathname]);

  const selectedKeys = router.pathname.substr(1) || 'sample';
  const defaultOpenKeys = selectedKeys.split('/')[1];

  return (
    <React.Fragment>
      <SidebarLogo/>
      <div className="gx-sidebar-content">
        <div className={`gx-sidebar-notifications ${getNoHeaderClass(navStyle)}`}>
          <UserProfile/>
          {/* <AppsNavigation/> */}
        </div>
        <CustomScrollbars className="gx-layout-sider-scrollbar">
          <Menu
            defaultOpenKeys={[defaultOpenKeys]}
            selectedKeys={[selectedKeys]}
            theme={themeType === THEME_TYPE_LITE ? 'lite' : 'dark'}
            mode="inline">
              <MenuItemGroup key="main" className="gx-menu-group" title={<IntlMessages id="sidebar.main"/>}>
                <Menu.Item key="dashboard">
                    <Link href={`/${DASHBOARD_PATH}`}>
                      <a><i className="icon icon-dasbhoard"/>
                        <span><IntlMessages id="sidebar.dashboard"/></span>
                      </a></Link>
                  </Menu.Item>
              </MenuItemGroup>

              <MenuItemGroup key="ecommerce" className="gx-menu-group" title={<IntlMessages id="sidebar.ecommerce"/>}>
                <Menu.Item key="categories">
                  <Link href={`/${DASHBOARD_PATH}ecommerce/categories`}>
                    <a><i className="icon icon-product-list"/>
                      <span><IntlMessages id="sidebar.ecommerce.categories"/></span>
                    </a></Link>
                </Menu.Item>
                <Menu.Item key="products">
                  <Link href={`/${DASHBOARD_PATH}ecommerce/products`}>
                    <a><i className="icon icon-pricing-table"/>
                      <span><IntlMessages id="sidebar.ecommerce.products"/></span>
                    </a></Link>
                </Menu.Item>
                <SubMenu key="brands_suppliers" popupClassName={getNavStyleSubMenuClass(navStyle)}
                       title={<span><i className="icon icon-company"/>
                         <span><IntlMessages id="sidebar.ecommerce.brands_suppliers"/></span></span>}>
                  <Menu.Item key="brands">
                    <Link href={`/${DASHBOARD_PATH}ecommerce/brands`}>
                      <a>
                        <span><IntlMessages id="sidebar.ecommerce.brands"/></span>
                      </a></Link>
                  </Menu.Item>
                  <Menu.Item key="suppliers">
                    <Link href={`/${DASHBOARD_PATH}ecommerce/suppliers`}>
                      <a>
                        <span><IntlMessages id="sidebar.ecommerce.suppliers"/></span>
                      </a></Link>
                  </Menu.Item>
                </SubMenu>

                <Menu.Item key="variations">
                  <Link href={`/${DASHBOARD_PATH}ecommerce/variations`}>
                    <a><i className="icon icon-filter-circle"/>
                      <span><IntlMessages id="sidebar.ecommerce.variations"/></span>
                    </a></Link>
                </Menu.Item>
                {/* <Menu.Item key="taxes">
                    <Link href={`/${DASHBOARD_PATH}ecommerce/taxes`}>
                      <a><i className="icon">%</i>
                        <span><IntlMessages id="sidebar.ecommerce.taxes"/></span>
                      </a></Link>
                </Menu.Item> */}
              </MenuItemGroup>

              <MenuItemGroup key="investment" className="gx-menu-group" title={<IntlMessages id="sidebar.investment"/>}>                
                <Menu.Item key="projects">
                  <Link href={`/${DASHBOARD_PATH}investment/projects`}>
                    <a><i className="icon icon-default-timeline"/>
                      <span><IntlMessages id="sidebar.investment.projects"/></span>
                    </a></Link>
                </Menu.Item>
              </MenuItemGroup>

              {/* <MenuItemGroup key="accounts" className="gx-menu-group" title={<IntlMessages id="sidebar.accounts"/>}>
                <Menu.Item key="clients">
                  <Link href={`/${DASHBOARD_PATH}accounts/clients`}>
                    <a><i className="icon icon-dasbhoard"/>
                      <span><IntlMessages id="sidebar.accounts.clients"/></span>
                    </a></Link>
                </Menu.Item>
                
                <Menu.Item key="employees">
                  <Link href={`/${DASHBOARD_PATH}accounts/employees`}>
                    <a><i className="icon icon-user"/>
                      <span><IntlMessages id="sidebar.accounts.employees"/></span>
                    </a></Link>
                </Menu.Item>
              </MenuItemGroup> */}
          </Menu>
        </CustomScrollbars>
      </div>
    </React.Fragment>
  );
};

SidebarContent.propTypes = {};
export default SidebarContent;

