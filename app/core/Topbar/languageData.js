const languageData = [
  {
    languageId: 'vietnam',
    locale: 'vi',
    name: 'Vietnam',
    icon: 'vn',
    code: 'vn'
  },
  {
    languageId: 'english',
    locale: 'en',
    name: 'English',
    icon: 'us',
    code: 'en'
  },
];
export default languageData;
