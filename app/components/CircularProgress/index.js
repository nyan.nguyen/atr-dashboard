import React from "react";

const CircularProgress = ({className}) => <div className={`loader ${className}`}>
  <img src="/images/white-logo.png" alt="loader" style={{width: 400}}/>
</div>;
export default CircularProgress;
CircularProgress.defaultProps = {
  className: ''
}
