import internals from '../util/httpClient';

export const getAll = (options) => internals.get('/investment',[],{...options});
export const create = (payload,options) => internals.post('/investment/create',payload,{...options});
export const get = (payload,options) => internals.get('/investment/get/'+payload._id,[],{...options});
export const update = (payload,options) => internals.post('/investment/update',payload,{...options});
export const remove = (payload,options) => internals.post('/investment/delete',payload,{...options});