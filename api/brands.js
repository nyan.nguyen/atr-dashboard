import internals from '../util/httpClient';

export const getBrands = (options) => internals.get('/brand',[],{...options});
export const createBrand = (payload,options) => internals.post('/brand/create',payload,{...options});
export const getBrand = (payload,options) => internals.get('/brand/get/'+payload.bid,[],{...options});
export const updateBrand = (payload,options) => internals.post('/brand/update',payload,{...options});
export const deleteBrand = (payload,options) => internals.post('/brand/delete',payload,{...options});